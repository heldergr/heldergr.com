'use strict';

/**
 * @ngdoc function
 * @name heldergrApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the heldergrApp
 */
angular.module('heldergrApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
