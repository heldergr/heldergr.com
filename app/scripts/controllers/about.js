'use strict';

/**
 * @ngdoc function
 * @name heldergrApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the heldergrApp
 */
angular.module('heldergrApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
