'use strict';

/**
 * @ngdoc overview
 * @name heldergrApp
 * @description
 * # heldergrApp
 *
 * Main module of the application.
 */
angular
  .module('heldergrApp', [
    'ngResource',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
